package celestials;

public class Position extends Coordinates {
	
	public Position(double x, double y){
		super(x,y);
	}
	
	public double distance(Position s2){
		double dx = this.x - s2.getX();
		double dy = this.y - s2.getY();
		return Math.sqrt(dy*dy + dx*dx);
	}
	
	public UnitVector direction(Position s2){
		double dx= s2.getX() - this.x;
		double dy= s2.getY() - this.y;
		double magnitude = Math.sqrt(dx*dx + dy*dy);
		return new UnitVector(dx/magnitude, dy/magnitude);
	}
	
	
	//move function to return distance after iteration to detect collision---Nevermind, this is better implemented by checking positions in Body class
	public void moveIteration(Velocity v, Acceleration a, double t){
		this.x = this.x + v.getX()*t + 0.5 * a.getX() * Math.pow(t, 2);
		this.y = this.y + v.getY()*t + 0.5 * a.getY() * Math.pow(t, 2);
	}

}
