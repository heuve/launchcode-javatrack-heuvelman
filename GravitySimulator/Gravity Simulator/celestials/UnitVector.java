package celestials;

public class UnitVector extends Coordinates{

	public UnitVector(double x, double y) {
		super(x,y);
	}
	
	public double angle(){
		double atan = Math.atan2(this.y, this.x);
		if(this.y >=0){
			return atan;
		}
		else{
			return atan + Math.PI;
		}
	}
	
	public double angleDeg(){
		return this.angle() * 180.0 / Math.PI;
	}
	
	public double angleCalc(UnitVector u){
		double dotProduct = this.x * u.getX() + this.y * u.getY();
		double angle = Math.acos(dotProduct);
		return angle;
	}
	
	public double angleCalcDeg(UnitVector u){
		return this.angleCalc(u) * 180.0 / Math.PI;
	}

}
