package pset9;


import java.math.RoundingMode;
import java.text.DecimalFormat;

public class BetterStudent {
	private String firstName;
	private String lastName;
	private int studentID;
	private int credits;
	private double gradePoints;
	private double GPA;
	private DecimalFormat df = new DecimalFormat("0.000");
	
	
	public BetterStudent(String firstName, String lastName, int studentID){
		this(firstName, lastName, studentID, 0, 0);
	}
	
	public BetterStudent(String firstName, String lastName, int studentID, int credits, float GPA){
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentID = studentID;
		this.credits = credits;
		this.GPA = GPA;
		this.gradePoints = GPA * credits;
	}
	
	public String getName(){
		return this.firstName + " " + this.lastName;
	}
	
	public int getID(){
		return studentID;
	}
	
	public int getCredits(){
		return credits;
	}
	
	public String getGPA(){
		this.df.setRoundingMode(RoundingMode.HALF_UP);
		this.GPA = this.gradePoints/this.credits;
		return this.df.format(GPA);
	}
	
	public String getClassStanding(){
		int year = credits/30;
		if (year > 3){
			year = 4;
		}
		
		switch(year){
		case 0: 
			return "Freshman";
		case 1:
			return "Sophomore";
		case 2:
			return "Junior";
		case 3:
			return "Senior";
		case 4:
			return "Senior*";
		default:
			return "N/A";
		}
	}

	public int submitGrade(int grade, int credits){
		if(grade < 0 || grade > 4)
			return 1;
		this.gradePoints = this.gradePoints + grade * credits;
		return 0;
	}
	
}




