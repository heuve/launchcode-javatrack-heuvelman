package pset9;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Student {
	private String firstName;
	private String lastName;
	private int studentID;
	private int credits;
	private double gradePoints;
	private double GPA;
	private DecimalFormat df = new DecimalFormat("0.000");
	
	
	public Student(String firstName, String lastName, int studentID){
		this(firstName, lastName, studentID, 0, 0.0);
	}
	
	public Student(String firstName, String lastName, int studentID, int credits, double GPA){
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentID = studentID;
		this.credits = credits;
		this.GPA = GPA;
		this.gradePoints = GPA * credits;
	}
	
	public String getName(){
		return this.firstName + " " + this.lastName;
	}
	
	public int getStudentID(){
		return studentID;
	}
	
	public int getCredits(){
		return credits;
	}
	
	public double getGPA(){
		double roundedGPA = Math.round(this.GPA * 1000) / 1000.0;
		return roundedGPA;
	}
	
	public String getFormattedGPA(){
		this.df.setRoundingMode(RoundingMode.HALF_UP);
		return this.df.format(GPA);
	}
	
	public String getClassStanding(){
		int year = credits/30;
		if (year > 3){
			year = 4;
		}
		
		switch(year){
		case 0: 
			return "Freshman";
		case 1:
			return "Sophomore";
		case 2:
			return "Junior";
		case 3:
			return "Senior";
		case 4:
			return "Senior*";
		default:
			return "N/A";
		}
	}

	public int submitGrade(double grade, int credits){
		if(grade < 0 || grade > 4)
			return 1;
		this.gradePoints = this.gradePoints + grade * credits;
		this.credits = this.credits + credits;
		this.GPA = this.gradePoints/this.credits;
		return 0;
	}
	
	public double computeTuition(){
		double semesters =  Math.ceil(this.credits / 15.0);
		double tuition = semesters * 20000.0;
		return tuition;
	}
	
	public String toString(){
		return (this.firstName + " " + this.lastName + " " + this.studentID);
	}
	
	public Student createLegacy(Student that){
		int credits;
		if(this.getCredits() > that.getCredits()){
			credits = this.getCredits();
		}
		else{
			credits = that.getCredits();
		}
		return new Student(this.getName(), that.getName(), this.getStudentID() + that.getStudentID(),
				credits, (this.getGPA()+that.getGPA())/2.0);
	}
	
}




