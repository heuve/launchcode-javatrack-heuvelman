package pset9;

import java.util.ArrayList;
import java.util.ListIterator;

public class Course {
	
	private String name;
	private int credits;
	private int capacity;
	private int enrolled;
	private ArrayList<Student> roster;
	
	public Course(String name, int credits, int capacity){
		this.name = name;
		this.credits = credits;
		this.capacity = capacity;
		this.enrolled = 0;
		roster = new ArrayList<Student>(this.capacity);
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getCredits(){
		return this.credits;
	}
	
	public int getCapacity(){
		return this.capacity;
	}
	
	public int getRemainingSeats(){
		return this.capacity - this.enrolled;
	}
	
	public void setName(String newName){
		this.name = newName;
	}
	
	public void setCredits(int credits){
		this.credits = credits;
	}
	
	public void setCapacity(int capacity){
		this.capacity = capacity;
	}
	
	public boolean addStudent(Student student){
		if (this.getRemainingSeats() > 0){
			if (!roster.contains(student)){
				roster.add(student);
				this.enrolled++;
				return true;
			}
			else {
				return false;
			}
		}
		return false;
	}
	
	public String generateRoster(){
	String rosterString = null;
	Student next;
	ListIterator<Student> iterator = roster.listIterator();
	while(iterator.hasNext()){
		next = iterator.next();
		if(next != null)
			rosterString = rosterString + next.getName() + " ";		
	}
	
	return rosterString;
	}
	
	public double averageGPA(){
		double gpa = 0.0;
		Student next;
		int count = 0;
		ListIterator<Student> iterator = roster.listIterator();
		while(iterator.hasNext()){
			next = iterator.next();
			if(next != null){
				gpa = gpa + next.getGPA();
				count++;
			}
		}
		return gpa/count;
	}
	
	public String toString(){
		return this.getName() + " " + this.getCredits() + " " + this.getRemainingSeats()+ "/" + this.getCapacity();
	}

}