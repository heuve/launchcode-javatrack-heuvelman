package april26studio;

public class Rectangle {
	private int height;
	private int width;
	
	public Rectangle(int newHeight, int newWidth){
		this.height = newHeight ;
		this.width = newWidth ;
	}
	
	public boolean isSquare(){
		if (this.width == this.height)
			return true;
		return false;
	}
	
	public int area(){
		return this.width * this.height ;
	}
	
	public int perimeter(){
		return this.width * 2 + this.height * 2 ;
	}
	
	public boolean isSmaller(Rectangle that){
		if (this.area() < that.area())
			return true;
		return false;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public int getWidth(){
		return this.width;
	}
}