package april26studio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BaseballPlayerTester {
	
	private BaseballPlayer a; 
	
	@Before
	public void setUp() {		
		a = new BaseballPlayer("Willie McGee", 51, "Both");
	}

	@Test
	public void testConstructor() {
		assertTrue("Bats Initialization Error", a.getBats()== "Both");
		assertTrue("Games Initialization Error", a.getGames() == 0);
		assertTrue("Hits Initialization Error", a.getHits() == 0);
		assertTrue("RBI Initialization Error", a.getRBI() == 0);
		assertTrue("Number Initialization Error", a.getNumber() == 51);
		assertTrue("Name Initialization Error", a.getName()== "Willie McGee");
	}
	
	@Test
	public void testGameUpdate(){
		a.gameUpdate(3, 5);
		assertTrue("Hits not updated correctly", a.getHits() == 3);
		assertTrue("RBI not updated correctly", a.getRBI() == 5);
		assertTrue("Game Update not incrementing games correctly", a.getGames() == 1);
	}
	
	@Test
	public void testGameUpdateSecond(){		
		a.gameUpdate(3, 5);
		a.gameUpdate(2, 1);
		assertTrue("Hits not updated correctly", a.getHits() == 5);
		assertTrue("RBI not updated correctly", a.getRBI() == 6);
		assertTrue("Game Update not incrementing games correctly", a.getGames() == 2);
	}

}
