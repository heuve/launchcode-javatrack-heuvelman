package april26studio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RectangleTester {

	private Rectangle r;
	private Rectangle s;
	private Rectangle t;
	
	@Before
	public void setUp(){
		r = new Rectangle(3,4);
		s = new Rectangle(4,4);
		t = new Rectangle(3,3);
	
	}
	
	@Test
	public void testConstructor() {
		assertTrue("Height is wrong", r.getHeight() == 3);
		assertTrue("width is wrong", r.getWidth() == 4);
	}

	@Test
	public void testArea() {
		assertTrue("Area calculated incorrectly", r.area() == 12);
	}
	
	@Test
	public void testPerimeter() {
		assertTrue("Perimeter calculated incorrectly", r.perimeter()== 14);
	}
	
	@Test
	public void testIsSmaller() {
		assertTrue("r should be smaller than s", r.isSmaller(s));
		assertFalse("r should be larger than t", r.isSmaller(t));
	}
	
	@Test
	public void testIsSquare() {
		assertTrue("s is a square", s.isSquare());
		assertFalse("r is not a square", r.isSquare());
		assertTrue("t is a square", t.isSquare());
	}
}
