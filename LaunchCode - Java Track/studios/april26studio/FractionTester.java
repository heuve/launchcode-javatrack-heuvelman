package april26studio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FractionTester {

	private Fraction f;
	private Fraction g;
	
	@Before
	public void setUp(){
		f = new Fraction(1,5);
		g = new Fraction(2,5);
	}
	
	@Test
	public void testConstructor() {
		assertTrue("Numerator constructed incorrectly", f.getNumerator() == 1);
		assertTrue("Denominator constructed incorrectly", f.getDenominator() == 5);
	}

	@Test
	public void testAddFraction() {
		Fraction h = f.addFraction(g);
		assertTrue("Fractions added incorrectly", h.getNumerator() == 3 && h.getDenominator() == 5);
	}
	
	@Test
	public void testMultiplyFraction() {
		Fraction h = f.multiplyFraction(g);
		assertTrue("Fractions multiplied incorrectly", h.getNumerator() == 2 && h.getDenominator() == 25);
		Fraction i = new Fraction(1,4);
		Fraction j = new Fraction(2,4);
		Fraction k = i.multiplyFraction(j);
		assertTrue("Fraction multiplication error--check simplification", k.getNumerator() == 1 && k.getDenominator() == 8);
	}
	
	@Test
	public void testRecripocal() {
		assertTrue("Numerator constructed incorrectly", f.recriprocal().getNumerator() == 5);
		assertTrue("Denominator constructed incorrectly", f.recriprocal().getDenominator() == 1);
	}
}
