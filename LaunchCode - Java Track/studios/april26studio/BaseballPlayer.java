package april26studio;

public class BaseballPlayer {
	private String name;
	private int jerseyNumber;
	private String bats;
	private int games;
	private int hits;
	private int RBI;
	
	public BaseballPlayer(String name, int jerseyNumber){
		this(name, jerseyNumber, "Right", 0, 0, 0);
	}
	
	public BaseballPlayer(String name, int jerseyNumber, String bats){
		this(name, jerseyNumber, bats, 0, 0, 0);
	}
	
	public BaseballPlayer(String name, int jerseyNumber, String bats, int games, int hits, int RBI){
		this.name= name;
		this.jerseyNumber = jerseyNumber;
		this.bats = bats;
		this.games = games;
		this.hits = hits;
		this.RBI = RBI;
	}
	
	public void gameUpdate(int hits, int RBI){
		this.games++ ;
		this.hits = this.hits + hits;
		this.RBI = this.RBI + RBI;
	}
	
	public int getGames(){
		return this.games;
	}
	
	public int getHits(){
		return this.hits;
	}
	
	public int getRBI(){
		return this.RBI;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getNumber(){
		return this.jerseyNumber;
	}
	
	public String getBats(){
		return this.bats;
	}
}
