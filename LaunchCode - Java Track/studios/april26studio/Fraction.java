package april26studio;

public class Fraction {
	private int numerator;
	private int denominator;
	
	public Fraction(int newNumerator, int newDenominator){
		this.numerator = newNumerator ;
		this.denominator = newDenominator ;
	}
	
	public Fraction addFraction(Fraction that){
		int resultNumerator;
		int resultDenominator;
		resultNumerator = this.numerator * that.denominator + 
				this.denominator * that.numerator;
		resultDenominator = this.denominator * that.denominator;
		Fraction unsimplifiedResult = new Fraction(resultNumerator, resultDenominator);
		return unsimplifiedResult.simplify();
	}
	
	public Fraction multiplyFraction(Fraction that){
		int resultNumerator;
		int resultDenominator;
		resultNumerator = this.numerator * that.numerator ;
		resultDenominator = this.denominator * that.denominator ;
		Fraction unsimplifiedResult = new Fraction(resultNumerator, resultDenominator);
		return unsimplifiedResult.simplify();
	}
	
	public Fraction recriprocal(){
		return new Fraction(this.denominator, this.numerator);
	}
	
	public Fraction simplify(){
		for(int i = this.numerator; i > 0; i--){
			if (this.numerator % i == 0 && this.denominator % i == 0){
				return new Fraction(this.numerator / i , this.denominator / i);
			}
		}
		return new Fraction(0,0);
	}
	
	public int getNumerator(){
		return this.numerator;
	}
	
	public int getDenominator(){
		return this.denominator;
	}
}
	
