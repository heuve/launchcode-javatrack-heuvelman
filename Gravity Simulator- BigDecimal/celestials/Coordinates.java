package celestials;

public class Coordinates {

	protected double x;
	protected double y;
	
	public Coordinates(double x, double y){
			this.x = x;
			this.y = y;
		}
		
	public void setX(double x){
			this.x=x;
		}
		
	public void setY(double y){
			this.y = y;
		}
		
	public double getX(){
			return x;
		}
		
	public double getY(){
			return y;
		}
	}


