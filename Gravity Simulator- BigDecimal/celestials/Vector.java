package celestials;

public class Vector {
	

	protected double x;
	protected double y;
	protected double magnitude;
	protected UnitVector unitVector;
	
	public Vector(double x, double y){
		this.x = x;
		this.y = y;
		this.updateUnitVector();
	}
	
	public Vector(double magnitude, UnitVector unitVector){
		this.magnitude = magnitude;
		this.unitVector = unitVector;
		this.updateComponentValues();
	}
	
	public void setX(double x){
		this.x=x;
		this.updateUnitVector();		
	}
	
	public void setY(double y){
		this.y = y;
		this.updateUnitVector();
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public void setMagnitude(double magnitude){
		this.magnitude = magnitude;
		this.updateComponentValues();
	}
	
	public double getMagnitude(){
		return this.magnitude;
	}
	
	public void setDirection(UnitVector unitVector){
		this.unitVector = unitVector;
		this.updateComponentValues();
	}
	
	public UnitVector getUnitVector(){
		return this.unitVector;
	}
	
	protected void updateUnitVector(){
		this.magnitude = Math.sqrt(this.x * this.x + this.y * this.y);
		this.unitVector = new UnitVector(this.x/this.magnitude, this.y/this.magnitude);
	}
	
	protected void updateComponentValues(){
		this.x = this.unitVector.getX()*this.magnitude;
		this.y = this.unitVector.getY()*this.magnitude;
	}
	
	public double angleCalc(Vector v){
		return this.unitVector.angleCalc(v.getUnitVector());
	}
	
	public double angleCalcDeg(Vector v){
		return this.unitVector.angleCalcDeg(v.getUnitVector());
	}
	
	public double getAngle(){
		return this.unitVector.angle();
	}
	
	public double getAngleDeg(){
		return this.unitVector.angleDeg();
	}
	
	/* Additional functionality for dot product, cross product, additional dimensions can be added when necessary */
}
