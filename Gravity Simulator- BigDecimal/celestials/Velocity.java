package celestials;

public class Velocity extends Vector{

		
	public Velocity(double x, double y){
		super(x,y);
	}
	
	public Velocity(double magnitude, UnitVector unitVector){
		super(magnitude, unitVector);		
	}
		
	public void accelerate(Acceleration a, double t){
		this.x = this.x + a.getX()*t;
		this.y = this.y + a.getY()*t;
		this.updateUnitVector();
	}
	
	public Velocity relativeV(Velocity v2){
		return new Velocity(this.x - v2.getX(), this.y - v2.getY());
	}
}
