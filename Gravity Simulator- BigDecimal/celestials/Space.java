package celestials;

import java.awt.EventQueue;

import javax.swing.JFrame;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JRadioButtonMenuItem;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.awt.event.ActionEvent;

public class Space {

	private JFrame frmGravitySimulator;
	private DefaultListModel<Body> listModel;
	private DefaultListModel<String> listModelStrings;
	private JList list;
	private final ButtonGroup timeScale = new ButtonGroup();
	private final ButtonGroup dtCompute = new ButtonGroup();
	private final ButtonGroup bounds = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Space window = new Space();
					window.frmGravitySimulator.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Space() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @return 
	 */
	private void initialize() {
		frmGravitySimulator = new JFrame();
		frmGravitySimulator.setTitle("Gravity Simulator");
		frmGravitySimulator.setBounds(100, 100, 746, 468);
		frmGravitySimulator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGravitySimulator.getContentPane().setLayout(null);
		listModel = new DefaultListModel<Body>();
		listModelStrings = new DefaultListModel<String>();
		JList list = new JList(listModelStrings);
		

		list.setBounds(15, 253, 694, 107);
		frmGravitySimulator.getContentPane().add(list);

		
		
		
		JButton btnGenerateCelestialBody = new JButton("Generate Celestial Body");
		btnGenerateCelestialBody.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if( listModel.size() < 2){
					String name = getName("What is the name of the body?","Name Celestial");
					double mass = getPositiveValue("What is " + name + "'s mass? (kg)", "Mass Dialog");
					double radius = getPositiveValue("What is " + name + "'s radius? (m)", "Radius Dialog");
					double sx = getValue("What is " + name + "'s x-position? (m)","x-Position");
					double sy = getValue("What is " + name + "'s y-position? (m)", "y-Position");
					double vx = getValue("What is the initial x-velocity of" + name + "? (m/s)", "x-Velocity");
					double vy = getValue("What is the initial y-velocity of" + name + "? (m/s)", "y-Velocity");					
					Body b = new Body(name, mass, radius, sx, sy, vx, vy);
					listModel.add(listModel.size(), b);
					listModelStrings.add(listModel.size()-1, b.getName());
				}
				else{
					JOptionPane.showMessageDialog(frmGravitySimulator, "Only two bodies are supported at this time", "Body Limit", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnGenerateCelestialBody.setBounds(15, 16, 694, 29);
		frmGravitySimulator.getContentPane().add(btnGenerateCelestialBody);
		
		JButton btnEdit = new JButton("Edit...");
		btnEdit.setBounds(15, 367, 115, 29);
		frmGravitySimulator.getContentPane().add(btnEdit);
		
		JLabel lblTime = new JLabel("Time");
		lblTime.setBounds(15, 115, 69, 20);
		frmGravitySimulator.getContentPane().add(lblTime);
		
		JRadioButton rdbtnSeconds = new JRadioButton("seconds");
		rdbtnSeconds.setSelected(true);
		timeScale.add(rdbtnSeconds);
		rdbtnSeconds.setBounds(95, 111, 105, 29);
		frmGravitySimulator.getContentPane().add(rdbtnSeconds);
		
		JRadioButton rdbtnHours = new JRadioButton("hours");
		timeScale.add(rdbtnHours);
		rdbtnHours.setBounds(400, 111, 87, 29);
		frmGravitySimulator.getContentPane().add(rdbtnHours);
		
		JRadioButton rdbtnMinutes = new JRadioButton("minutes");
		timeScale.add(rdbtnMinutes);
		rdbtnMinutes.setBounds(239, 111, 105, 29);
		frmGravitySimulator.getContentPane().add(rdbtnMinutes);
		
		JRadioButton rdbtnDays = new JRadioButton("days");
		timeScale.add(rdbtnDays);
		rdbtnDays.setBounds(544, 111, 155, 29);
		frmGravitySimulator.getContentPane().add(rdbtnDays);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selected = list.getSelectedIndex();
				listModel.remove(selected);
				listModelStrings.remove(selected);
				/*if((selected == 0) && (listModel.get(1) != null)){
					listModel.add(0, listModel.get(1));
					listModel.remove(1);
					listModelStrings.add(0, listModelStrings.get(1));
					listModelStrings.remove(1);*/
				//}
			}
		});
		btnRemove.setBounds(140, 367, 115, 29);
		frmGravitySimulator.getContentPane().add(btnRemove);
		
		JLabel lblSimulation = new JLabel("Simulation");
		lblSimulation.setBounds(15, 70, 694, 29);
		frmGravitySimulator.getContentPane().add(lblSimulation);
		
		JLabel lblDt = new JLabel("dt");
		lblDt.setBounds(15, 163, 69, 20);
		frmGravitySimulator.getContentPane().add(lblDt);
		
		JRadioButton rdbtnAutomatic = new JRadioButton("Automatic");
		rdbtnAutomatic.setSelected(true);
		dtCompute.add(rdbtnAutomatic);
		rdbtnAutomatic.setBounds(11, 195, 131, 29);
		frmGravitySimulator.getContentPane().add(rdbtnAutomatic);
		
		JRadioButton rdbtnUserdefined = new JRadioButton("User-Defined");
		dtCompute.add(rdbtnUserdefined);
		rdbtnUserdefined.setBounds(140, 195, 155, 29);
		frmGravitySimulator.getContentPane().add(rdbtnUserdefined);
		
		JLabel lblBoundaries = new JLabel("Boundaries");
		lblBoundaries.setBounds(354, 163, 95, 20);
		frmGravitySimulator.getContentPane().add(lblBoundaries);
		
		JRadioButton rdbtnAutomatic_1 = new JRadioButton("Automatic");
		rdbtnAutomatic_1.setSelected(true);
		bounds.add(rdbtnAutomatic_1);
		rdbtnAutomatic_1.setBounds(354, 195, 155, 29);
		frmGravitySimulator.getContentPane().add(rdbtnAutomatic_1);
		
		JRadioButton rdbtnUserdefined_1 = new JRadioButton("User-Defined");
		bounds.add(rdbtnUserdefined_1);
		rdbtnUserdefined_1.setBounds(511, 195, 155, 29);
		frmGravitySimulator.getContentPane().add(rdbtnUserdefined_1);
		
		JButton solar = new JButton("Solar");
		solar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listModel.clear();
				listModelStrings.clear();
				listModel.add(0, new Body("sun", BigDecimal.valueOf(198855, -25), 
						696300000, -454043, 0, 0, -0.0904));
				listModel.add(1, new Body("Earth", BigDecimal.valueOf(597237, -19), 
						6371000, 149597416657., 0, 0,29784.8 ));
				listModelStrings.add(0,"sun");
				listModelStrings.add(1,"Earth");
			}
		});
		solar.setBounds(311, 367, 115, 29);
		frmGravitySimulator.getContentPane().add(solar);
		
		JButton lunar = new JButton("Lunar");
		lunar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listModel.clear();
				listModelStrings.clear();
				listModel.add(0, new Body("Earth", BigDecimal.valueOf(597237, -19), 
						6371000, 0, 0, 0, 0));
				listModel.add(1, new Body("moon", BigDecimal.valueOf(734767309, -14), 
						1737000, 384400000, 0, 0, 1022));
				listModelStrings.add(0,"Earth");
				listModelStrings.add(1,"moon");
			}
		});
		lunar.setBounds(436, 367, 115, 29);
		frmGravitySimulator.getContentPane().add(lunar);
		
		JButton btnSimulate = new JButton("Simulate");
		btnSimulate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listModel.size()!= 2 || listModel.get(0)== null || listModel.get(1)==null){
					JOptionPane.showMessageDialog(frmGravitySimulator, 
							"Please ensure two Bodies have been generated", 
							"Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				double time = getPositiveValue("Time in seconds", "Time Dialog");
				double dt;
				if(rdbtnUserdefined.isSelected()){
					dt = getPositiveValue("Time increments for simulation (in seconds)", "dt Setting");
				}
				else{
					dt = time / 100000000.0;
				}
				
				Body a = listModel.get(0);
				Body b = listModel.get(1);
				a.simulate(b, time, dt);
			}
		});
		btnSimulate.setBounds(594, 367, 115, 29);
		frmGravitySimulator.getContentPane().add(btnSimulate);
		
		
	}

	private String getName(String prompt, String title){
		return (String)JOptionPane.showInputDialog(frmGravitySimulator, prompt, title, JOptionPane.PLAIN_MESSAGE,
				null, null, "");
	}
	
	private double getPositiveValue(String prompt, String title){
		String s = (String)JOptionPane.showInputDialog(frmGravitySimulator, prompt, title, JOptionPane.PLAIN_MESSAGE,
				null, null, "");
		double n = Double.parseDouble(s);
		while(n < 0){
			s = (String)JOptionPane.showInputDialog(frmGravitySimulator, prompt + " (Please enter a positive value)", title, JOptionPane.PLAIN_MESSAGE,
					null, null, "");
			n= Double.parseDouble(s);
		}
		return n;
	}
	
	private double getValue(String prompt, String title){
		String s = (String)JOptionPane.showInputDialog(frmGravitySimulator, prompt, title, JOptionPane.PLAIN_MESSAGE,
				null, null, "");
		double n = Double.parseDouble(s);
		return n;
	}
}
