package celestials;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.math.MathContext;

public class Body {

	private String name;
	private BigDecimal mass;
	private double radius;
	private Position s;
	private Velocity v;
	private Acceleration a;
	public static BigDecimal G = BigDecimal.valueOf(667408, 16);

	
	public Body(String name, BigDecimal mass, double radius, double sx, double sy, double vx, double vy){
		this.mass = mass;
		this.radius = radius;
		this.name = name;
		this.s = new Position(sx, sy);
		this.v = new Velocity(vx, vy);
		this.a = new Acceleration(0,0);
	}
	
	public Body(String name, double mass, double radius, double sx, double sy, double vx, double vy){
		this.mass = new BigDecimal(mass);
		this.radius = radius;
		this.name = name;
		this.s = new Position(sx, sy);
		this.v = new Velocity(vx, vy);
		this.a = new Acceleration(0,0);
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public BigDecimal getMass(){
		return this.mass;
	}
	
	public double getRadius(){
		return this.radius;
	}
	
	public void setMass(BigDecimal mass){
		this.mass = mass;
	}
	
	public void setRadius(double radius){
		this.radius = radius;
	}
	
	public Position getPosition(){
		return this.s;
	}
	
	public Velocity getVelocity(){
		return this.v;
	}
	
	public void setPosition(Position s){
		this.s = s;
	}
	
	public void setVelocity(Velocity v){
		this.v = v;
	}

	public Acceleration getAcceleration(){
		return this.a;
	}
	
	public void setAcceleration(Acceleration a){
		this.a = a;
	}
	
	public boolean gravitate(Body that, double dt){
		/*Force thisF = new Force (G * this.mass * that.getMass() / Math.pow(this.s.distance(that.getPosition()),2), this.s.direction(that.getPosition()));
		Force thatF = new Force (-thisF.getX(), -thisF.getY());		
		this.calcAcceleration(thisF);
		that.calcAcceleration(thatF);
		*/
		
		this.updateAcceleration(that);
		that.updateAcceleration(this);
		this.moveIteration(dt);
		that.moveIteration(dt);
		
		if (!this.checkCollision(that)){
			this.accelerate(dt);
			that.accelerate(dt);
			return true;
		}
		else{
			return false;
		}
	}
	
	
/*	public void calcAcceleration(Force F){
		this.a.calculate(F, this.mass);
	}
*/	
	
	public void updateAcceleration(Body that){
		BigDecimal d = new BigDecimal(this.s.distance(that.getPosition()));
		BigDecimal dSquared = d.pow(2, MathContext.DECIMAL32);
		BigDecimal massDivDSquared = that.getMass().divide(dSquared, MathContext.DECIMAL32);
		BigDecimal magnitude = G.multiply(massDivDSquared);
		this.a.setMagnitude(magnitude.doubleValue());
		this.a.setDirection(this.s.direction(that.getPosition()));
	}
	
	
	public void moveIteration(double dt){
		this.s.moveIteration(this.v, this.a, dt);
	}
	
	public boolean checkCollision(Body that){
		double clipping = this.radius + that.getRadius();
		double distance = this.s.distance(that.getPosition());
		if(distance < clipping){
			return true;
		}else{
			return false;
		}
	}
	
	public void accelerate(double dt){
		this.v.accelerate(this.a, dt);
	}
	
	public void collisionHandler(Body that, double t){
		DecimalFormat df = new DecimalFormat("#0.00");
		UnitVector direction = this.s.direction(that.getPosition());
		UnitVector velocity = this.v.relativeV(that.getVelocity()).getUnitVector();
		double collisionX = this.s.getX() + direction.getX() * this.radius;
		double collisionY = this.s.getY() + direction.getY() * this.radius;
		double impactAngle = this.impactAngle(direction, velocity);
		
		System.out.println("These objects will collide after " + df.format(t) + " seconds at location (" + df.format(collisionX) +", " + df.format(collisionY) +")");
		System.out.println("These objects will impact at an angle of " + df.format(impactAngle) + " degrees.");
	}
	
	public void simulate(Body that, double time, double dt){
		DecimalFormat df = new DecimalFormat("#0.00");
		int n = (int)(time / dt);
		Velocity relativeV;
		for(int i = 0; i < n; i++){
			if(!this.gravitate(that, dt)){
				this.collisionHandler(that, i*dt);
				break;
			}
		/*	if(i%10000 == 0){
				System.out.println("Earth:");
				System.out.println("v: " + this.v.x + " " + this.v.y);
				System.out.println("s: " + this.s.getX() + " " + this.s.getY());
				System.out.println(" ");
				System.out.println("moon:");
				System.out.println("v: " + that.v.x + " " + that.v.y);
				System.out.println("s: " + that.s.getX() + " " + that.s.getY());
				System.out.println(" ");*/
				
			//}
		}
		
		relativeV = this.v.relativeV(that.getVelocity());
		
		System.out.println(this.name);
		System.out.println("Final Position: (" + df.format(this.s.getX()) + " , "
				+ df.format(this.s.getY()) + ")");
		System.out.println("Final Velocity: " + df.format(this.v.getMagnitude()) + " <" +
				df.format(this.v.getX()) + " , " + df.format(this.v.getY()) + ">");
		System.out.println("");
		System.out.println(that.name);
		System.out.println("Final Position: (" + df.format(that.s.getX()) + " , "
				+ df.format(that.s.getY()) + ")");
		System.out.println("Final Velocity: " + df.format(that.v.getMagnitude()) + " <" +
				df.format(that.v.getX()) + " , " + df.format(that.v.getY()) + ">");
		System.out.println("");
		System.out.println("Final distance between bodies (center of mass):");
		System.out.println(df.format(this.s.distance(that.s)));
		System.out.println("");
		System.out.println("Final relative velocity between bodies:");		
		System.out.println(df.format(relativeV.getMagnitude()));
		
	}
	
	private double impactAngle(UnitVector orientation, UnitVector velocity){
		double angle = orientation.angleCalcDeg(velocity) - 90;
		return Math.abs(angle);
	}
	
	public static void main(String args[]){
		Body Earth = new Body("Earth", 5.972* Math.pow(10, 24), 6371000, 0, 0, 0, 0);
		Body moon = new Body("moon", 7.34767309 * Math.pow(10, 22), 1737000, 384400000, 0, 0, 1022 );
		//Body sun = new Body("sun")
		
		/*System.out.println(Earth.s.getX() + " " + Earth.s.getY());
		System.out.println(moon.s.getX() + " " + moon.s.getY());
		System.out.println(Earth.s.distance(moon.s));
		System.out.println(moon.v.magnitude);
		System.out.println(Earth.v.magnitude);
		System.out.println(" ");
		
		Earth.simulate(moon, 2333000, 1);
		System.out.println(Earth.s.getX() + " " + Earth.s.getY());
		System.out.println(moon.s.getX() + " " + moon.s.getY());
		System.out.println(Earth.s.distance(moon.s));
		System.out.println(moon.v.magnitude);
		System.out.println(Earth.v.magnitude);*/
	
	}

}
