package celestials;

public class Acceleration extends Vector {

	public Acceleration(double x, double y) {
		super(x, y);
	}

	public Acceleration(double magnitude, UnitVector unitVector) {
		super(magnitude, unitVector);
	}
	
	public void calculate(Force F, double mass){
		this.x = F.getX() / mass;
		this.y = F.getY() / mass;
		this.updateUnitVector();
	}
	
}
