package celestials;

public class Force extends Vector {

	public Force(double x, double y) {
		super(x, y);
	}

	public Force(double magnitude, UnitVector unitVector) {
		super(magnitude, unitVector);
	}

}
